﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace GoogleGeotag
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Place> places = new List<Place>();
            List<Task<Place>> placesTask = new List<Task<Place>>();
            Place local;
            Task<Place> localTask;

            Console.WriteLine("Program starting...");
            Console.WriteLine("Gathering data...");
            
            //Create Tasks
            foreach (string item in readFromFile("famousplaces.txt"))
            {
                placesTask.Add(Process(item));
                Thread.Sleep(200);
            }

            foreach (Task<Place> item in placesTask)
            {
                item.GetAwaiter().GetResult();
                if (item.Result != null)
                    places.Add(item.Result);
            }

            // Get local Data
            Console.WriteLine("Where are u now?");
            string localStr = Console.ReadLine();
            localTask = Process(localStr);
            localTask.GetAwaiter().GetResult();
            local = localTask.Result;
            Console.WriteLine("Processing...");

            // Closest to position
            Console.WriteLine("Closest to your position:");
            var q_closest = (from x in places
                             let dis = local.distanceBetweenCoordinates(x.Lat, x.Lng)
                             orderby dis
                             select new Place(x.Name, x.FormattedAddress, x.Country, x.Lat, x.Lng)).ToList();
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine(q_closest[i].ToString());
            }
            Console.WriteLine("---------------------------------------------------------------");
            
            // Most sights in one country
            Console.WriteLine("Most sights in one country");
            var q_mostSights = (from x in places
                               group x by x.Country into c
                               orderby c.Count() descending
                               select new { country = c.Key, sightsNum = c.Count() }).FirstOrDefault();
            Console.WriteLine(q_mostSights.country + " : " + q_mostSights.sightsNum);
            Console.WriteLine("---------------------------------------------------------------");

            // 2 closest to each other
            Console.WriteLine("2 closest to each other");
            var q_2closest = (from x in places
                              from y in places
                              let d = x.distanceBetweenCoordinates(y.Lat, y.Lng)
                              where x.FormattedAddress != y.FormattedAddress
                              orderby d
                              select new { s1 = x.FormattedAddress, s2 = y.FormattedAddress }).FirstOrDefault();
            Console.WriteLine(q_2closest.s1 + "\n" + q_2closest.s2 );
            Console.WriteLine("---------------------------------------------------------------");

            // The 2 farthest sights in the same country
            Console.WriteLine("The 2 farthest sights in the same country");
            var q_2farthest = (from x in places
                               from y in places
                               let d = x.distanceBetweenCoordinates(y.Lat, y.Lng)
                               where x.FormattedAddress != y.FormattedAddress && x.Country == y.Country
                               orderby d descending
                               select new { s1 = x.FormattedAddress, s2 = y.FormattedAddress }).FirstOrDefault();
            Console.WriteLine(q_2farthest.s1 + "\n" + q_2farthest.s2);

            Console.ReadKey();
            
        }

        static List<string> readFromFile(string filename)
        {
            List<string> lines = new List<string>();
            StreamReader sr = new StreamReader(filename);
            while (!sr.EndOfStream)
            {

                lines.Add(sr.ReadLine());
            }
            return lines;
        }



        static async Task<Place> Process(string placeName)
        {
            string s = await new HttpClient().GetStringAsync("https://maps.googleapis.com/maps/api/geocode/xml?address=" + placeName);
            XDocument xd = XDocument.Parse(s);

            var q = (from x in xd.Descendants("GeocodeResponse")
                     where x.Element("status").Value == "OK"
                     from y in x.Descendants("result")
                     let fa = y.Element("formatted_address").Value
                     from c in x.Descendants("address_component")
                     where c.Element("type").Value == "country"
                     let country = c.Element("long_name").Value
                     from z in x.Descendants("address_component")
                     from k in x.Descendants("location")
                     let lat = double.Parse(k.Element("lat").Value.Replace('.', ','))
                     let lng = double.Parse(k.Element("lng").Value.Replace('.', ','))
                     select new Place(placeName, fa, country, lat, lng )).FirstOrDefault();

            return q;
        }
    }
}
