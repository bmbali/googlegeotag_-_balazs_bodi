﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace GoogleGeotag
{
    class Place
    {
        public string Name { get; set; }
        public string FormattedAddress { get; set; }
        public string Country { get; set; }
        public double Lat { get; set; }
        public double Lng { get; set; }

        public Place(string name)
        {
            Name = name;
            FormattedAddress = "";
            Country = "";
            Lat = 0;
            Lng = 0;
        }

        public Place(string name, string formattedAddress, string country, double lat, double lng)
        {
            Name = name;
            FormattedAddress = formattedAddress;
            Country = country;
            Lat = lat;
            Lng = lng;
        }

        static double degreesToRadians(double degrees)
        {
            return degrees * Math.PI / 180;
        }

        public double distanceBetweenCoordinates(double lat1, double lon1)
        {
            double lat2 = Lat, lon2 = Lng;
            var earthRadiusKm = 6371;

            var dLat = degreesToRadians(lat2 - lat1);
            var dLon = degreesToRadians(lon2 - lon1);

            lat1 = degreesToRadians(lat1);
            lat2 = degreesToRadians(lat2);

            var a = Math.Sin(dLat / 2) * Math.Sin(dLat / 2) +
                    Math.Sin(dLon / 2) * Math.Sin(dLon / 2) * Math.Cos(lat1) * Math.Cos(lat2);
            var c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
            return earthRadiusKm * c;
        }

        public override string ToString()
        {
            return Name + ":\n\t address: " + FormattedAddress + " country :" + Country + "\n\t lat " + Lat + " lng " + Lng;
        }
    }
}
